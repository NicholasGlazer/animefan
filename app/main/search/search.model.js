(function() {
    'use strict';

    angular
        .module('main').factory("Search", ['$resource', 'SERVER_URL', function($resource, SERVER_URL) {
            return $resource(SERVER_URL + '?search=:query', null,
                {
                    find: {method: 'GET', isArray: true, timeout:500, cache: false}
                });
        }]);
})();