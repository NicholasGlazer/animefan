(function () {
    'use strict';
    angular.module('main')
        .controller('searchCtrl', searchCtrl);

    function searchCtrl($scope, Search) {
        var vm = this;

		$scope.data = {};
		vm.searchAnime = ionic.debounce(function() {
			if ($scope.data.sQuery !== '') {
				Search.find({query: $scope.data.sQuery}, function(res) {
					$scope.searchResult = res;
				});
			}
		}, 500);  

		vm.clearSearch = function() {
			$scope.data.sQuery = '';
			$scope.searchResult = null;
		};

        return vm;
    }
})();