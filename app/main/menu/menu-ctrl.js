(function () {
    'use strict';
    angular.module('main')
        .controller('menuCtrl', menuCtrl);

    /** @ngInject */
    function menuCtrl($scope, $ionicModal, $ionicPopover, $timeout) {
        var vm = this;
		$scope.doRefresh = function () {
			// console.log('refesshing');
			setTimeout(function() {
				$scope.$broadcast('scroll.refreshComplete');
			}, 1500);
		};

		$ionicPopover.fromTemplateUrl('main/menu/menu-popover.html', {
			scope: $scope
		}).then(function(popover) {
			$scope.popover = popover;
		});

		$scope.openPopover = function($event) {
			$scope.popover.show($event);
		};

		// Form data for the login modal
		$scope.loginData = {};

		// Create the login modal that we will use later
		$ionicModal.fromTemplateUrl('main/components/login.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		// Triggered in the login modal to close it
		$scope.closeLogin = function() {
		$scope.modal.hide();
		};

		// Open the login modal
		$scope.login = function() {
		$scope.modal.show();
		};

		// Perform the login action when the user submits the login form
		$scope.doLogin = function() {
		// console.log('Doing login', $scope.loginData);

		// Simulate a login delay. Remove this and replace with your login
		// code if using a login system
			$timeout(function() {
				$scope.closeLogin();
			}, 1000);
		};

        return vm;
    }
})();