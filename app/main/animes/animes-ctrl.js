(function() {
	'use strict';
	angular.module('main')
		.controller('animesCtrl', animesCtrl);

	/** @ngInject */
	function animesCtrl($scope, Anime) {
		var vm = this;

		$scope.animeList = Anime.query();

		return vm;
	}
})();