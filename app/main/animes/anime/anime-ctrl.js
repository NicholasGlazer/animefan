(function() {
	'use strict';
	angular.module('main')
		.controller('animeCtrl', animeCtrl);
	/** @ngInject */
	function animeCtrl($scope, $stateParams, AnimeSingle) {
		var vm = this;
		vm.animeSingle = AnimeSingle;

		$scope.select = $scope.selectModel;

		// $scope.longList  = [];
		// for(var i=0;i<1000; i++){
		//   $scope.longList.push(i);
		// }
		return vm;
	}
})();