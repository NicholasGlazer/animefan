(function() {
    'use strict';

    angular
        .module('main').factory("Anime", ['$resource', 'SERVER_URL', function($resource, SERVER_URL) {
            return $resource(SERVER_URL + '/:id', { id:'@_id', title:'@title' },
                {
                  'query': { method: 'GET', isArray: true },
                  'get'  : { method: 'GET', isArray: false }
                });
        }]);
})();