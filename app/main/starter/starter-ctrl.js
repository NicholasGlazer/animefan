(function () {
    'use strict';
    angular.module('main')
        .controller('starterCtrl', starterCtrl);

    /** @ngInject */
    function starterCtrl($scope, Anime) {
        var vm = this;

		vm.animeList = Anime.query();

        return vm;
    }
})();