'use strict';
angular.module('main', [
  'ionic',
  'ionic-modal-select',
  'ngCordova',
  'ngResource',
  'ui.router',
  'ionic.contrib.ui.hscrollcards'
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider, $urlRouterProvider) {

  // ROUTING with ui.router
  $urlRouterProvider.otherwise('/main/starter');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/menu/menu.html',
      controller: 'menuCtrl',
      controllerAs: 'menu',
      resolve: {
        Animes: function ($q, Anime) {
          var deferred = $q.defer();
          Anime.query( function (res) {
            var animes = res;
            deferred.resolve(animes);
            // console.log(animes);
          });
          return deferred.promise;
        }
      }
    })
      .state('main.starter', {
        url: '/starter',
        views: {
          'pageContent': {
            templateUrl: 'main/starter/starter.html',
            controller: 'starterCtrl',
            controllerAs: 'starter'
          }
        }
      })
      .state('main.search', {
        url: '/search',
        views: {
          'pageContent': {
            templateUrl: 'main/search/search.html',
            controller: 'searchCtrl',
            controllerAs: 'search'
          }
        }
      })
      .state('main.settings', {
        url: '/settings',
        views: {
          'pageContent': {
            templateUrl: 'main/settings/settings.html',
            controller: 'settingsCtrl',
            controllerAs: 'settings'
          }
        }
      })
      .state('main.animes', {
        url: '/animes',
        views: {
          'pageContent': {
            templateUrl: 'main/animes/animes.html',
            controller: 'animesCtrl',
            controllerAs: 'animes'
          }
        }
      })
      .state('main.anime', {
        url: '/animes/:id',
        views: {
          'pageContent': {
            templateUrl: 'main/animes/anime/anime.html',
            controller: 'animeCtrl',
            controllerAs: 'anime'
          }
        }
        ,
        resolve: {
          AnimeSingle: function ($stateParams, $q, Anime) {
            var deferred = $q.defer();
            Anime.get({ id: $stateParams.id }, function (res) {
              deferred.resolve(res);
              console.log(res);
            });
            return deferred.promise;
          }
        }
      })
      ;
});
